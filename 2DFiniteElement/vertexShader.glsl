#version 330 core
layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec3 vertexColor;

out vec3 fragColor;

uniform float aspectRatio=1;

void main()
{
    gl_Position = vec4(vertexPosition * vec3( 1.0f, aspectRatio,1.0)*2., 1.0);
    fragColor = vertexColor;
}