//
//  Mesh.cpp
//  2DFiniteElement
//
//  Created by Christian Cosgrove on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//

#include "Mesh.h"
#include <fstream>

#include <iostream>
Mesh::Mesh(const std::string& fileName)
{
    std::ifstream stream(fileName, std::ios::in);
    
    std::string line;
    while (std::getline(stream, line))
    {
        std::size_t prefixEnd = line.find(' ');
        std::string prefix = line.substr(0,prefixEnd+1);
        std::string suffix = line.substr(prefixEnd+1);
        if (prefix=="v ")
        {
            glm::vec3 v;
            size_t currentSize=vertices.size();
            sscanf(suffix.c_str(), "%f %f %f", &v.x, &v.y, &v.z);
            vertices.push_back(Vertex{glm::vec2(v.x, v.z), glm::vec3(1,0,0)}); //Blender uses Y as height instead of Z.  I've converted here.
            //Create element for each vertex.
            elements.push_back(Element(currentSize,v.y));
        
        }
        else if (prefix=="f ")
        {
            int i, j, k;
            sscanf(suffix.c_str(), "%i %i %i", &i, &j, &k);
            
            if (i-1>vertices.size() || j-1>vertices.size() || k-1>vertices.size() ||
                i-1<0 || j-1<0 || k-1<0) throw std::out_of_range("Out of range");
            
            indices.push_back(i-1);
            indices.push_back(j-1);
            indices.push_back(k-1);
        }
    }
    for (size_t i = 0; i<elements.size();++i) elements[i].InitializeElement(i,vertices, elements,indices);
    int numNeighbors=0;
    for (Element& element : elements) numNeighbors+=element.neighbors.size();
    std::cout << "Average number of neighbors " << (float)numNeighbors/elements.size() <<std::endl;
    std::cout << "Num elements " << elements.size() << std::endl;
    
    generateBuffers();
    glBindVertexArray(vao);
    updateBuffers();
    
    glBindVertexArray(0);
}

void Mesh::generateBuffers()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)__offsetof(Vertex, position));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)__offsetof(Vertex, color));
    
    glBindVertexArray(0);
}

void Mesh::solve()
{
    const float omega = 1.1;
    for (int m=0;m<10;++m)
    {
        for (int i = 0; i<elements.size();++i)
            if (elements[i].BoundaryValue!=0) elements[i].Temperature=elements[i].BoundaryValue;
        for (int i = 0; i<elements.size();++i)
        {
            float sigma = 0;
            Element* elem = &elements[i];
//            for (int j = 0; j<elem->neighbors.size();++j)
//            {
//                Element* currNeighbor = &elements[elements[i].neighbors[j].neighborIndex];
//                int neighborIndex2=-1;
//                for (int k = 0; k < currNeighbor->neighbors.size();++k)
//
//                {
//                    if (currNeighbor->neighbors[k].neighborIndex==i) neighborIndex2 = k;
//                }
//                if (neighborIndex2==-1) continue;
//                if (neighborIndex2>=elements[elements[i].neighbors[j].neighborIndex].neighbors.size()) throw std::out_of_range("Out of neighbor range");
//                float add =elements[elements[i].neighbors[j].neighborIndex].Temperature * glm::dot(elements[i].GetGradient(vertices, j), elements[elements[i].neighbors[j].neighborIndex].GetGradient(vertices, neighborIndex2));
//                std::cout << add << std::endl;
//                if (!std::isnan(add)) sigma+=add;
//                
            //            }
            for (Element::Neighbor& neighbor : elem->neighbors)
            {
                sigma+=neighbor.gradientDot * neighbor.area * elements[neighbor.neighborIndex].Temperature;
            }
            float self =elements[i].SelfGradientDotArea;
//            printf("self: %f\n", self);
            elem->Temperature+=omega * ((5 - sigma)/self - elem->Temperature);
        }
    }
    for (Vertex& vertex:vertices) vertex.color=glm::vec3(0,0,0);
    float averageTemp=0;
    for (Element& element : elements)
    {
        if (!std::isnan(element.Temperature))
        {
            vertices[element.v1].color=renderColor(element.Temperature*1);
            averageTemp+=element.Temperature;
        }
    }
    printf("Average temperature %f\n", averageTemp/elements.size());
}

void Mesh::RenderMesh()
{
    
    glBindVertexArray(vao);
    updateBuffers();
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, (void*)0);
    
    glBindVertexArray(0);
    
}

void Mesh::updateBuffers()
{
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), &indices[0], GL_STATIC_DRAW);
}
