//
//  Vertex.h
//  2DFiniteElement
//
//  Created by Christian Cosgrove on 12/9/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#pragma once
#include "glm/glm.hpp"
struct Vertex
{
    glm::vec2 position;
    glm::vec3 color;
};