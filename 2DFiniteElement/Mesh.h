//
//  Mesh.h
//  2DFiniteElement
//
//  Created by Christian Cosgrove on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#include "Element.h"
#include <string>
#include <vector>
#include <OpenGL/gl3.h>
#include "Vertex.h"
class Mesh
{
public:
    Mesh(const std::string& fileName);
    std::vector<Element> elements;
    inline float GetInnerProduct(size_t e1, size_t e2);
    void RenderMesh();
    void solve();
    
private:
    GLuint vbo,ibo, vao;
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    void generateBuffers();
    void updateBuffers();
    inline glm::vec3 renderColor(float temperature);
};

float Mesh::GetInnerProduct(size_t e1, size_t e2)
{
    return 0;
}

glm::vec3 Mesh::renderColor(float temperature)
{
    float stress = temperature*temperature;
    float variance=0.45;
    return glm::vec3(stress,stress,stress);
}