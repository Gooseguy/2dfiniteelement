//
//  h
//  2DFiniteElement
//
//  Created by Christian Cosgrove on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
#include "glm/glm.hpp"
#include <iostream>
#include <vector>

#include "Vertex.h"

class Element
{
public:
    struct Neighbor
    {
        size_t parentIndex, neighborIndex, v2, v3;
        float area, gradientDot;
        glm::vec2 gradient;
        Neighbor(size_t _parentIndex, size_t _neighborIndex, size_t _v2, size_t _v3, std::vector<Vertex>& vertices, std::vector<Element>& elements);
        glm::vec2 GetGradient(std::vector<Vertex>& vertices, std::vector<Element>& elements);
        float GetGradientDot(std::vector<Vertex>& vertices, std::vector<Element>& elements);
        float GetArea(std::vector<Vertex>& vertices, std::vector<Element>& elements);
    };
    size_t v1;
    std::vector<Neighbor> neighbors;
    
    
    float Temperature=0;
    float SelfGradientDotArea;
    float TotalArea;
    
    void InitializeElement(size_t elementIndex, std::vector<Vertex>& vertices, std::vector<Element>& elements, std::vector<unsigned int> &indices);
//    bool IsBoundary;
    float BoundaryValue=0;
    
    Element(size_t _v1);
    Element(size_t _v1, float boundaryValue);
    inline void Print(std::vector<Vertex>& vertices);
    
    float GetSelfGradientDot(std::vector<Vertex>& vertices, std::vector<Element>& elements);
private:
};

void Element::Print(std::vector<Vertex>& vertices)
{
    for (Neighbor& neighbor : neighbors)
    {
        std::cout <<
            vertices[v1].position.x << " " << vertices[v1].position.y << ", " <<
            vertices[neighbor.v2].position.x << " " << vertices[neighbor.v2].position.y << ", " <<
            vertices[neighbor.v3].position.x << " " << vertices[neighbor.v3].position.y << std::endl;
    }
}

inline float Element::Neighbor::GetArea(std::vector<Vertex> &vertices, std::vector<Element>& elements)
{
    glm::vec3 vv1(vertices[elements[parentIndex].v1].position,0);
    glm::vec3 vv2(vertices[v2].position,0);
    glm::vec3 vv3(vertices[v3].position,0);
    return glm::length(glm::cross(vv2-vv1,vv3-vv2))/2;
}
