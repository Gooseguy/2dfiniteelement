//
//  MainGame.cpp
//  2DFiniteElement
//
//  Created by Christian on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//

#include "MainGame.h"
#include <OpenGL/gl3.h>
#include <SDL2/SDL.h>
#include "GLManager.h"
#include "ResourcePath.hpp"
#include "Mesh.h"


MainGame::MainGame() : CurrentState(ProgramState::RUN), WINDOW_WIDTH(1280), WINDOW_HEIGHT(720)
{
    
    init();
    GLManager glManager(resourcePath() + "fragmentShader.glsl", resourcePath() + "vertexShader.glsl");
    glManager.Programs[0].SetFloat("aspectRatio", (float)WINDOW_WIDTH / WINDOW_HEIGHT);
    Mesh mesh(resourcePath() + "bridge.obj");
    
    
    while (CurrentState!=ProgramState::EXIT)
    {
        mesh.solve();
        update();
        draw(mesh);
        handleEvents();
        SDL_GL_SwapWindow(window);
        
        
    }
}

void MainGame::init()
{
    if (SDL_Init(SDL_INIT_VIDEO)) throw std::logic_error ("Failed to initialize SDL!  " + std::string(SDL_GetError()));
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetSwapInterval(1);
    
    
    window = SDL_CreateWindow("Finite element steady state heat equation", 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL);
    if (window==nullptr) throw std::logic_error("Failed to initialize window");
    SDL_GLContext context = SDL_GL_CreateContext(window);
    if (context == nullptr) throw std::logic_error("Failed to initialize OpenGL context");
    glClearColor(0.0,0.0,0.0,1.0);
}


void MainGame::draw(Mesh& mesh)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    mesh.RenderMesh();
}

void MainGame::update()
{
    
}

void MainGame::handleEvents()
{
    SDL_Event event;
    
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                CurrentState = ProgramState::EXIT;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode)
            {
                case SDL_SCANCODE_ESCAPE:
                    CurrentState = ProgramState::EXIT;
                    break;
                default:break;
            }
        }
    }
}