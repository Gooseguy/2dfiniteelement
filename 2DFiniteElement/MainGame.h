//
//  MainGame.h
//  2DFiniteElement
//
//  Created by Christian on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//
class SDL_Window;
class Mesh;

class MainGame
{
public:
    enum class ProgramState { RUN, EXIT } CurrentState;
    MainGame();
private:
    const int WINDOW_WIDTH, WINDOW_HEIGHT;
    SDL_Window* window;
    void init();
    void update();
    void draw(Mesh& mesh);
    void handleEvents();
};