//
//  Element.cpp
//  2DFiniteElement
//
//  Created by Christian Cosgrove on 12/8/14.
//  Copyright (c) 2014 Christian Cosgrove. All rights reserved.
//

#include "Element.h"

Element::Element(size_t _v1) : v1(_v1)
{
}
Element::Element(size_t _v1, float boundaryValue) : v1(_v1), BoundaryValue(boundaryValue)
{
}

Element::Neighbor::Neighbor(size_t _parentIndex, size_t _neighborIndex, size_t _v2, size_t _v3, std::vector<Vertex>& vertices, std::vector<Element>& elements) : parentIndex(_parentIndex), neighborIndex(_neighborIndex), v2(_v2), v3(_v3), area(GetArea(vertices, elements)), gradientDot(GetGradientDot(vertices, elements)), gradient(GetGradient(vertices, elements))
{
    
}
void Element::InitializeElement(size_t elementIndex, std::vector<Vertex>& vertices, std::vector<Element> &elements, std::vector<unsigned int>& indices)
{
    std::vector<std::pair<size_t, size_t>> neig;
    for (int i = 0; i<indices.size();i+=3)
    {
        size_t v2,v3;
        size_t x=indices[i+0],y=indices[i+1],z=indices[i+2];
        if (x==v1) v2=y,v3=z;
        if (y==v1) v2=x,v3=z;
        if (z==v1) v2=x,v3=y;
        if (x==v1 ||y==v1 || z==v1)
        {
            neig.push_back(std::pair<size_t,size_t>(v2,v3));
        }
    }
    //find elements sharing vertices.
    for (size_t i = 0; i<elements.size();++i)
    {
        if (i==elementIndex)continue;
        for (auto& pair : neig)
        {
            if (pair.first==elements[i].v1) neighbors.push_back(Neighbor(elementIndex,i,pair.first,pair.second, vertices, elements));
            else if (pair.second==elements[i].v1) neighbors.push_back(Neighbor(elementIndex,i,pair.second, pair.first, vertices, elements));
        }
    }
    SelfGradientDotArea = GetSelfGradientDot(vertices, elements);
    TotalArea=0;
    for (Neighbor& neighbor : neighbors) TotalArea+=neighbor.area;
}

float Element::GetSelfGradientDot(std::vector<Vertex> &vertices, std::vector<Element>& elements)
{
    float tot=0;
    for (Neighbor& neighbor : neighbors)
    {
        glm::vec2 gradient = neighbor.GetGradient(vertices, elements);
        tot+=glm::dot(gradient,gradient)*neighbor.area;
    }
    return tot;
}


//Returns the gradient of the basis function centered at v1 on the element.  This function's dot product with other nearby elements makes up the stiffness matrix elements.
glm::vec2 Element::Neighbor::GetGradient(std::vector<Vertex>& vertices, std::vector<Element>& elements)
{
    //assuming that z(v1)=1 and z(v2)=0 and z(v3)=0
    glm::vec3 p1(vertices[elements[parentIndex].v1].position, 1);
    glm::vec3 p2(vertices[v2].position, 0);
    glm::vec3 p3(vertices[v3].position, 0);
    glm::vec3 normal = glm::cross(p2-p1,p3 - p1);
    if (normal.z<0)normal*=-1;
//    printf("grad %f, %f, %f\n", normal.x, normal.y, normal.z);
    return glm::vec2(-normal.x/normal.z,-normal.y/normal.z);
}

float Element::Neighbor::GetGradientDot(std::vector<Vertex> &vertices, std::vector<Element> &elements)
{
    
    //assuming that z(v1)=1 and z(v2)=0 and z(v3)=0
    glm::vec2 p1(vertices[elements[parentIndex].v1].position);
    if (elements[neighborIndex].v1!=v2) throw std::logic_error("Neighbor indices not corresponding!");
    glm::vec2 p2=vertices[v2].position;
    glm::vec2 p3=vertices[v3].position;
    glm::vec3 normal1(glm::cross(glm::vec3(p2-p1,-1), glm::vec3(p3-p1,-1)));
    if (normal1.z<0)normal1*=-1;
    glm::vec2 d1(normal1.x,normal1.y);
    glm::vec3 normal2(glm::cross(glm::vec3(p1-p2,-1), glm::vec3(p3-p2,-1)));
    if (normal2.z<0)normal2*=-1;
    glm::vec2 d2(normal2.x, normal2.y);
//    glm::vec3 normal3(glm::cross(glm::vec3(p1-p3,-1), glm::vec3(p2-p3,-1)));
//    glm::vec2 d3(-normal3.x/normal3.z,-normal3.y/normal3.z);
    if (std::isnan(glm::dot(d1,d2))) throw std::logic_error("Nan results!");
    else if (glm::dot(d1,d2)!=0) printf("Dot: (%f, %f), (%f, %f)\n", d1.x,d1.y,d2.x,d2.y);
    return glm::dot(d1,d2);
}
